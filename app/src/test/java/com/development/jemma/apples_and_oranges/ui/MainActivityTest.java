package com.development.jemma.apples_and_oranges.ui;

import com.development.jemma.apples_and_oranges.model.Fruit;

import org.junit.Test;

import static org.hamcrest.CoreMatchers.is;
import static org.junit.Assert.assertThat;


public class MainActivityTest {

    final MainActivity mainActivity = new MainActivity();

    /**
     * Buy one get one free on Apples
     * 1 apple = 0.60
     */
    @Test
    public void calculateTotalCostOfApplesWithOffer() {
        Fruit fruit = Fruit.APPLE;
        assertThat(mainActivity.calculateTotalCostWithOffers(fruit, 0), is(0.0));
        assertThat(mainActivity.calculateTotalCostWithOffers(fruit, 1), is(0.60));
        assertThat(mainActivity.calculateTotalCostWithOffers(fruit, 2), is(0.60));
        assertThat(mainActivity.calculateTotalCostWithOffers(fruit, 3), is(1.20));
        assertThat(mainActivity.calculateTotalCostWithOffers(fruit, 4), is(1.20));
        assertThat(mainActivity.calculateTotalCostWithOffers(fruit, 20), is(6.0));
    }

    /**
     * 3 for the price of 2 on Oranges
     * 1 orange = 0.25
     */
    @Test
    public void calculateTotalCostOfOrangesWithOffer() {
        Fruit fruit = Fruit.ORANGE;
        assertThat(mainActivity.calculateTotalCostWithOffers(fruit, 0), is(0.0));
        assertThat(mainActivity.calculateTotalCostWithOffers(fruit, 1), is(0.25));
        assertThat(mainActivity.calculateTotalCostWithOffers(fruit, 2), is(0.50));
        assertThat(mainActivity.calculateTotalCostWithOffers(fruit, 3), is(0.50));
        assertThat(mainActivity.calculateTotalCostWithOffers(fruit, 4), is(0.75));
        assertThat(mainActivity.calculateTotalCostWithOffers(fruit, 20), is(3.50));
    }
}
