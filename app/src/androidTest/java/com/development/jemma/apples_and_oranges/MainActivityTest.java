package com.development.jemma.apples_and_oranges;

import android.support.test.espresso.contrib.RecyclerViewActions;
import android.support.test.rule.ActivityTestRule;
import android.support.test.runner.AndroidJUnit4;
import android.view.View;

import com.development.jemma.apples_and_oranges.model.Fruit;
import com.development.jemma.apples_and_oranges.ui.MainActivity;

import org.hamcrest.Description;
import org.hamcrest.Matcher;
import org.hamcrest.TypeSafeMatcher;
import org.junit.Rule;
import org.junit.Test;
import org.junit.runner.RunWith;

import java.util.Arrays;
import java.util.List;

import static android.support.test.espresso.Espresso.onView;
import static android.support.test.espresso.action.ViewActions.click;
import static android.support.test.espresso.assertion.ViewAssertions.matches;
import static android.support.test.espresso.matcher.ViewMatchers.isDisplayed;
import static android.support.test.espresso.matcher.ViewMatchers.withId;
import static android.support.test.espresso.matcher.ViewMatchers.withText;

@RunWith(AndroidJUnit4.class)
public class MainActivityTest {

    @Rule
    public ActivityTestRule<MainActivity> main = new ActivityTestRule<>(MainActivity.class);

    @Test
    public void checkAllViewsAreShowing() {
        List<Fruit> fruitData = Arrays.asList(Fruit.values());

        for (int i = 0; i < fruitData.size(); i++) {
            String priceText = main.getActivity().getResources()
                    .getString(R.string.text_price, fruitData.get(i).price);
            String quantityText = main.getActivity().getResources()
                    .getString(R.string.text_quantity, fruitData.get(i).quantity);

            onView(withId(R.id.recycler_items))
                    .perform(RecyclerViewActions.scrollToPosition(i));
            onView(withText(fruitData.get(i).name))
                    .check(matches(isDisplayed()));
            onView(withText(priceText))
                    .check(matches(isDisplayed()));
            onView(withIndex(withText(quantityText), i))
                    .check(matches(isDisplayed()));
        }

        String totalPriceText = main.getActivity().getResources().getString(R.string.text_price, 0.0);
        onView(withId(R.id.text_total))
                .check(matches(withText(totalPriceText)))
                .check(matches(isDisplayed()));
    }

    /**
     * Example selection: [ Apple, Apple, Orange, Apple ] => £1.45
     * (Includes offer of buy one get one free on apples)
     */
    @Test
    public void checkExampleSelectionProducesCorrectTotal() {
        onView(withText(Fruit.APPLE.name))
                .perform(click());
        onView(withText(Fruit.APPLE.name))
                .perform(click());
        onView(withText(Fruit.ORANGE.name))
                .perform(click());
        onView(withText(Fruit.APPLE.name))
                .perform(click());

        onView(withId(R.id.text_total))
                .check(matches(withText("£1.45")));
    }

    /**
     * Matcher to help test item views which share the same id and text within a RecyclerView
     */
    private static Matcher<View> withIndex(final Matcher<View> matcher, final int index) {
        return new TypeSafeMatcher<View>() {
            int currentIndex = 0;

            @Override
            public void describeTo(Description description) {
                description.appendText("with index: ");
                description.appendValue(index);
                matcher.describeTo(description);
            }

            @Override
            public boolean matchesSafely(View view) {
                return matcher.matches(view) && currentIndex++ == index;
            }
        };
    }
}
