package com.development.jemma.apples_and_oranges.ui;

import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.development.jemma.apples_and_oranges.R;
import com.development.jemma.apples_and_oranges.model.Fruit;

import java.util.ArrayList;
import java.util.List;

public class FruitAdapter extends RecyclerView.Adapter<FruitAdapter.FruitHolder> {

    private List<Fruit> fruits;
    private ClickListener clickListener;

    public FruitAdapter() {
        this.fruits = new ArrayList<>();
    }

    public void setClickListener(ClickListener clickListener) {
        this.clickListener = clickListener;
    }

    @Override
    public FruitHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.item_fruit, parent, false);
        return new FruitHolder(view);
    }

    @Override
    public void onBindViewHolder(final FruitHolder holder, final int position) {
        Fruit fruit = fruits.get(position);
        holder.fruit = fruit;
        holder.name.setText(fruit.name);
        holder.price.setText(holder.itemView.getResources().getString(R.string.text_price, fruit.price));
        holder.quantity.setText(holder.itemView.getResources().getString(R.string.text_quantity, fruit.quantity));
    }

    @Override
    public int getItemCount() {
        return fruits.size();
    }

    public void setFruits(List<Fruit> list) {
        fruits = list;
    }

    class FruitHolder extends RecyclerView.ViewHolder {

        public TextView name;
        public TextView price;
        public TextView quantity;

        public Fruit fruit;

        public FruitHolder(View itemView) {
            super(itemView);
            name = itemView.findViewById(R.id.text_fruit_name);
            price = itemView.findViewById(R.id.text_fruit_price);
            quantity = itemView.findViewById(R.id.text_quantity);
            itemView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    if (clickListener != null) {
                        clickListener.onFruitItemClicked(fruit);
                    }
                }
            });
        }
    }

    public interface ClickListener {
        void onFruitItemClicked(Fruit fruit);
    }

}

