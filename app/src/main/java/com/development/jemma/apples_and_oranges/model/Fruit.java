package com.development.jemma.apples_and_oranges.model;

public enum Fruit {
    APPLE("Apple", 0.60, 0, 1),
    ORANGE("Orange", 0.25, 0, 2);

    public String name;
    public double price;
    public int quantity;
    public int offerQuantity; // quantity at which you get the next one free

    Fruit(String name, double price, int quantity, int offerQuantity) {
        this.name = name;
        this.price = price;
        this.quantity = quantity;
        this.offerQuantity = offerQuantity;
    }
}
