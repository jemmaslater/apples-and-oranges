package com.development.jemma.apples_and_oranges.ui;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.widget.TextView;

import com.development.jemma.apples_and_oranges.R;
import com.development.jemma.apples_and_oranges.model.Fruit;

import java.util.ArrayList;
import java.util.List;

public class MainActivity extends AppCompatActivity implements FruitAdapter.ClickListener {

    RecyclerView fruitRecycler;
    TextView totalText;
    RecyclerView.LayoutManager layoutManager;
    FruitAdapter fruitAdapter;
    List<Fruit> fruits;
    int applesInCart;
    int orangesInCart;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        fruitRecycler = findViewById(R.id.recycler_items);
        totalText = findViewById(R.id.text_total);

        layoutManager = new LinearLayoutManager(this);
        fruitRecycler.setLayoutManager(layoutManager);

        fruitAdapter = new FruitAdapter();
        fruitAdapter.setClickListener(this);
        fruits = new ArrayList<>();
        fruits.add(Fruit.APPLE);
        fruits.add(Fruit.ORANGE);
        fruitAdapter.setFruits(fruits);

        fruitRecycler.setAdapter(fruitAdapter);

        // Initial cart is empty
        applesInCart = 0;
        orangesInCart = 0;
        // Initial total is £0.00
        totalText.setText(getResources().getString(R.string.text_price, 0.0));
    }

    @Override
    public void onFruitItemClicked(Fruit fruit) {
        fruits.get(fruits.indexOf(fruit)).quantity++;
        fruitAdapter.notifyDataSetChanged();
        switch (fruit) {
            case APPLE:
                applesInCart++;
                break;
            case ORANGE:
                orangesInCart++;
                break;
            default:
                break;
        }
        totalText.setText(getResources().getString(R.string.text_price,
                calculateTotalCostWithOffers(Fruit.APPLE, applesInCart)
                        + calculateTotalCostWithOffers(Fruit.ORANGE, orangesInCart)));
    }

    protected double calculateTotalCostWithOffers(Fruit fruit, int quantity) {
        int extraItems = quantity % (fruit.offerQuantity + 1);
        int fruitItems = quantity - extraItems;
        return (fruit.price * fruitItems * fruit.offerQuantity/(fruit.offerQuantity + 1))
                + (fruit.price * extraItems);
    }

}
